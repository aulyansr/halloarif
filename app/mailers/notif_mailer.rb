class NotifMailer < ApplicationMailer
  default from: "Hallo Arif <no-reply@halloarif.id>"
  def replymotivator(user)
    @user = user
    mail( :to => user.email,
      :subject => 'Ada Balasan Dari Motivator',
      :display_name => "Hallo Arif" )
  end
  def replyclient(user)
    @user = user
    mail( :to => user.email,
      :subject => 'Ada Balasan Dari Client',
      :display_name => "Hallo Arif" )
  end
  def newchat
    mail( :to => "adistyeky@gmail.com",
      :subject => 'Ada Chat Baru',
      :display_name => "Hallo Arif" )
  end
  def newbox
    mail( :to => "adistyeky@gmail.com",
      :subject => 'Ada Box Uneg Uneg Baru',
      :display_name => "Hallo Arif" )
  end

  def newschedule(user)
    @user = user
    mail( :to => "adistyeky@gmail.com",
      :subject => 'Ada Pembuatan Jadwal Baru',
      :display_name => "Hallo Arif" )
  end
  def updateschedule(user)
    @user = user
    mail( :to => "adistyeky@gmail.com",
      :subject => 'Ada Perubahan Jadwal Baru',
      :display_name => "Hallo Arif" )
  end
  def reminder_morning(user)
    @user = user
    mail( :to => (user.detail_motivators.last.email_pribadi rescue user.detail_motivators),
      :subject => 'Reminder Motivator',
      :display_name => "Hallo Arif" )
  end

  def schedule_reminder(user)
    @user = user
    mail( :to => (user.detail_motivators.last.email_pribadi rescue user.detail_motivators),
      :subject => 'Reminder Schedule Motivator',
      :display_name => "Hallo Arif" )
  end
end
