$('.home-slider').owlCarousel({
  autoplayHoverPause:false,
  autoplayTimeout:6000,
  loop:true,
  dots: false,

  responsive:{
    0:{
      autoWidth:false,
      mouseDrag: false,
      center:false,
      items:1
    },
    600:{
      nav:true,
      items:3
    },
    1000:{
      autoWidth:true,
      center:true,
      nav:true,
      items:3
    }
  }

})

$('.testimoni-slider').owlCarousel({
    center: true,
    items:3,
    loop:true,
    margin:10,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:3
      }
    }
});

$('.dashboard-slider').owlCarousel({
    center: true,
    items:1,
    loop:true,
    margin:10,
});

$('.pckg-slider').owlCarousel({
  loop:true,
  margin:10,
  dots: true,
  responsive:{
    0:{
      autoWidth:false,
      mouseDrag: false,
      center:false,
      items:1
    },
    600:{
      nav:true,
      items:1
    },
    1000:{
      nav:true,
      items:4
    }
  }
});

// $(function () {
//   if (jQuery(window).width() > 900) {
//     $(document).scroll(function () {
//       var $nav = $(".navbar");
//       $nav.toggleClass('scrolled shadow', $(this).scrollTop() > $nav.height());
//     });
//   }
// });

$("a.grouped_elements").fancybox();
// $('#box-popupup').modal('show');
$('.card-title').matchHeight();
$('.bx-testi').matchHeight();


(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$( ".custom-select" ).select2({
  theme: "bootstrap"
});
sendEvent0= function() {
  $('#demo-modal').trigger('next.m.1');
}
sendEvent1 = function() {
  if (!$("input[name='message_box[question_1]']:checked").val()) {
    alert('Pilih Salah Satu');
    $('.step-2').addClass('disabled');
  }
  else {
    $('#demo-modal').trigger('next.m.2');
  }
}
sendEvent2 = function() {
  if (!$("input[name='message_box[problem_category_id]']:checked").val()) {
    alert('Pilih Salah Satu');
    $('.step-3').addClass('disabled');
  }
  else {
    $('#demo-modal').trigger('next.m.3');
  }
}
sendEvent3 = function() {
  if (!$("input[name='message_box[question_2]']:checked").val()) {
    alert('Pilih Salah Satu');
    $('.step-3').addClass('disabled');
  }
  else {
    $('#demo-modal').trigger('next.m.4');
  }
}

var sedih = $('<span />', {
  'id': 'appended',
      'text': 'SEDIH ?'
});

var marah = $('<span />', {
  'id': 'appended',
      'text': 'MARAH ?'
});

var hampa = $('<span />', {
  'id': 'appended',
      'text': 'HAMPA ?'
});

var tertekan = $('<span />', {
  'id': 'appended',
      'text': 'TERTEKAN ?'
});


var stres = $('<span />', {
  'id': 'appended',
      'text': 'STRESS ?'
});

var bahagia = $('<span />', {
  'id': 'appended',
      'text': 'BAHAGIA ?'
});


$('input:radio[name="message_box[question_1]"]').change(

  function(){
    if ($(this).val() == 'sedih') {
      $(sedih).appendTo('#feel');
    } else if ($(this).val() == 'marah'){
      $(marah).appendTo('#feel');
    }
    else if ($(this).val() == 'hampa'){
      $(hampa).appendTo('#feel');
    }
    else if ($(this).val() == 'tertekan'){
      $(tertekan).appendTo('#feel');
    }
    else if ($(this).val() == 'stress'){
      $(stres).appendTo('#feel');
    }
    else if ($(this).val() == 'bahagia'){
      $(bahagia).appendTo('#feel');
    }

  });

  $('input:radio[name="message_box[problem_category_id]"]').change(

    function(){
      if ($(this).val() == '1') {
        $('.dtl').hide();
        $('#dtl-percintaan').show();
      } else if ($(this).val() == '2'){
        $('.dtl').hide();
        $('#dtl-keuangan').show();
      }
      else if ($(this).val() == '3'){
        $('.dtl').hide();
          $('#dtl-keluarga').show();
      }
      else if ($(this).val() == '4'){
        $('.dtl').hide();
          $('#dtl-pertemanan').show();
      }
      else if ($(this).val() == '5'){
        $('.dtl').hide();
          $('#dtl-diri').show();
      }
      else if ($(this).val() == '6'){
        $('.dtl').hide();
          $('#dtl-studi').show();
      }

    });

    $('.selectize').selectize({
      create: function(input) {
        return {
          value: input,
          text: input
        }
      }
    });

    $(document).on('click', 'a[href^="#"]', function(e) {
        // target element id
        var id = $(this).attr('href');

        // target element
        var $id = $(id);
        if ($id.length === 0) {
            return;
        }

        // prevent standard hash navigation (avoid blinking in IE)
        e.preventDefault();

        // top position relative to the document
        var pos = $id.offset().top - 50;

        // animated top scrolling
        $('body, html').animate({scrollTop: pos});
    });


    jQuery(document).ready(function() {
        var token = jQuery("#snap_token").val();
        jQuery('.order-button').on('click', function() {
            snap.pay(token, {
                onSuccess: function(res) {
                    console.log("SUCCESS", res);
                    window.location.replace(res.finish_redirect_url)
                },
                onPending: function(res) {
                    console.log("PENDING", res);
                },
                onError: function(res) {
                    console.log("ERROR", res);
                }
            })
        })
    });

$(".list-video").mCustomScrollbar();
$(".video-0,.video-1, .video-2, .video-3, .video-4, .video-5, .video-6, .video-7, .video-8,.video-9,.video-10,.video-11, .video-12, .video-13, .video-14, .video-15, .video-16, .video-17, .video-18").on("click", function(event) {
  event.preventDefault();
  $(".video_case iframe").prop("src", $(event.currentTarget).attr("href"));
});
    function openNav() {
      document.getElementById("mySidenav").style.width = "250px";
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
    }

    $('#all-feed').socialfeed({

           instagram:{
             accounts: ['@halloarif.id'],  //Array: Specify a list of accounts from which to pull posts
             limit: 6,                                    //Integer: max number of posts to load
             client_id: '78f6218dcc244d6e80776e1f021c8cb5',
             access_token: '6164484289.78f6218.d40902a34c194f5e8ff437a8e84ccc44'       //String: Instagram client id (optional if using access token)
           },

           // GENERAL SETTINGS
           length:400,                                     //Integer: For posts with text longer than this length, show an ellipsis.
           show_media:true,                                //Boolean: if false, doesn't display any post images
           media_min_width: 300,                           //Integer: Only get posts with images larger than this value
                                  //Integer: Number of seconds before social-feed will attempt to load new posts.

           template_html:
           '<div class="col-md-4 p-10 mb-4"> \
             <a href="{{=it.link}}" target="_blank"> \
              <img src="{{=it.attachment}}" class="w-100"> \
             </a> \
             </div>'                           //String: HTML used for each post. This overrides the 'template' filename option
         });
