class BeforeBoxController < ApplicationController
  include Wicked::Wizard
  steps :question_1, :category, :question_2, :box_uneg_uneg

  def show
    @conversation = MessageBox.where(user_id: current_user.id).last
    render_wizard
  end

  def update
    @conversation = MessageBox.where(user_id: current_user.id).last
    @conversation.status = "init"
    @conversation.update_attributes(conversation_params)

      require 'uri'
      require 'net/http'

      url = URI("https://reguler.zenziva.net/apps/smsapi.php?userkey=57z0sk&passkey=rYQFPeWHbX&nohp=081285545703&pesan=test%20kirim%20sms%20dengan%20zenziva%20api")

      http = Net::HTTP.new(url.host, url.port)

    render_wizard @conversation
  end


  private

  def finish_wizard_path
    finish_message_box_path(clt: "true")
  end

  def conversation_params
    params.require(:conversation).permit(:message, :question_1,:question_2, :problem_category_id, :status )
  end

end
