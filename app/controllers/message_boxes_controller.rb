class MessageBoxesController < ApplicationController
  before_action :set_message_box, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'admin'
  # GET /message_boxes
  # GET /message_boxes.json
  def index
    if current_user.is_motivator?
      @message_boxes = MessageBox.where(motivator_id:current_user.id)
    else
      @message_boxes = MessageBox.all
    end
  end

  # GET /message_boxes/1
  # GET /message_boxes/1.json
  def show
  end

  # GET /message_boxes/new
  def new
    @message_box = MessageBox.new
  end

  # GET /message_boxes/1/edit
  def edit
  end

  # POST /message_boxes
  # POST /message_boxes.json
  def create
    @message_box = MessageBox.new(message_box_params)

    respond_to do |format|
      if @message_box.save
        format.html { redirect_to user_path(current_user), notice: 'Curhatan Kamu Berhasil Di Kirim. Tunggu Balasan Dari Motivator' }
        NotifMailer.newbox.deliver
        format.json { render :show, status: :created, location: @message_box }
      else
        format.html { render :new }
        format.json { render json: @message_box.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /message_boxes/1
  # PATCH/PUT /message_boxes/1.json
  def update
    respond_to do |format|
      if @message_box.update(message_box_params)
        if current_user.is_human_capital?
          format.html { redirect_to finish_message_box_path(hct: @message_box.id) , notice: "Message box was successfully updated. #{@message_box.user}" }
        elsif current_user.is_motivator?
          format.html { redirect_to finish_message_box_path(mtv: @message_box.id)  }
          NotifMailer.replymotivator(@message_box.user).deliver
          unless NotificationDatum.where(id: @message_box.user.notif_id).count == 0
            @notification_data = NotificationDatum.find(@message_box.user.notif_id)
            Webpush.payload_send(endpoint: @notification_data.endpoint,
              message: "Hai, #{@message_box.user.full_name}. Ada balasan dari motivator. Segera masuk ke dashboard kamu ",
              p256dh: @notification_data.p256dh_key,
              auth: @notification_data.auth_key,
              ttl: 12 * 60 * 60,
              vapid: {
                subject: 'mailto:aulya@gmail.com',
                expiration: 12 * 60 * 60,
                public_key: $vapid_public,
                private_key: $vapid_private,
                })
              end
            else current_user.is_client?
              if @message_box.rating.nil?
                format.html { redirect_to finish_message_box_path(clt: @message_box.id)  }
              else
                format.html { redirect_to finish_message_box_path(fns: @message_box.id)  }
              end
            end
            format.json { }
          else
            format.html { render :edit }
            format.json { render json: @message_box.errors, status: :unprocessable_entity }
          end
        end
      end

      def sendPush
        notif_data = NotificationDatum.create(endpoint: params[:subscription][:endpoint],
          p256dh_key: params[:subscription][:keys][:p256dh],
          auth_key: params[:subscription][:keys][:auth])

          user = current_user.update_attributes(auth_key: params[:subscription][:keys][:auth], :notif_id => notif_data.id)
          render body: nil
        end

        def sendPayload(user)
          @message = get_message(user)
          if user.notif_id.present?

            puts "success"
          else
            puts "failed"
          end
        end

        def get_message(user)
          "hai #{user.full_name}, ada balasan dari motivator"
        end

        # DELETE /message_boxes/1
        # DELETE /message_boxes/1.json
        def destroy
          @message_box.destroy
          respond_to do |format|
            format.html { redirect_to message_boxes_url, notice: 'Message box was successfully destroyed.' }
            format.json { head :no_content }
          end
        end

        def init_box
          x = current_user
          y = MessageBox.new
          y.user_id = x.id
          y.save
          redirect_to before_box_path(:question_1)
        end

        def finish
          #code
        end

        private
        # Use callbacks to share common setup or constraints between actions.
        def set_message_box
          @message_box = MessageBox.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def message_box_params
          params.require(:message_box).permit(:user_id, :motivator_id, :status, :message, :message_reply, :problem_category_id, :question_1, :question_2, :rating, :reason)
        end
      end
