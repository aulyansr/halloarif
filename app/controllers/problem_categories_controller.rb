class ProblemCategoriesController < ApplicationController
  before_action :set_problem_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'admin'
  # GET /problem_categories
  # GET /problem_categories.json
  def index
    @problem_categories = ProblemCategory.all
  end

  # GET /problem_categories/1
  # GET /problem_categories/1.json
  def show
  end

  # GET /problem_categories/new
  def new
    @problem_category = ProblemCategory.new
  end

  # GET /problem_categories/1/edit
  def edit
  end

  # POST /problem_categories
  # POST /problem_categories.json
  def create
    @problem_category = ProblemCategory.new(problem_category_params)

    respond_to do |format|
      if @problem_category.save
        format.html { redirect_to @problem_category, notice: 'Problem category was successfully created.' }
        format.json { render :show, status: :created, location: @problem_category }
      else
        format.html { render :new }
        format.json { render json: @problem_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /problem_categories/1
  # PATCH/PUT /problem_categories/1.json
  def update
    respond_to do |format|
      if @problem_category.update(problem_category_params)
        format.html { redirect_to @problem_category, notice: 'Problem category was successfully updated.' }
        format.json { render :show, status: :ok, location: @problem_category }
      else
        format.html { render :edit }
        format.json { render json: @problem_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /problem_categories/1
  # DELETE /problem_categories/1.json
  def destroy
    @problem_category.destroy
    respond_to do |format|
      format.html { redirect_to problem_categories_url, notice: 'Problem category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_problem_category
      @problem_category = ProblemCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def problem_category_params
      params.require(:problem_category).permit(:name, :descriptions)
    end
end
