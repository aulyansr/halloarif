class MessagesController < ApplicationController
  before_action :set_message, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'admin', only: [:edit]
  # GET /messages
  # GET /messages.json
  def index
    @messagex = Message.new
    if current_user.is_motivator?
      @conversation =  Conversation.where(motivator_id: current_user.id).where(id: params[:conversation_id]).last
    elsif current_user.is_client?
      if params[:conversation_id].present?
        @conversation = Conversation.where(id: params[:conversation_id]).where(user_id: current_user.id).last
      else
        @conversation = current_user.conversations.last
      end
      @message = current_user.conversations.last.messages.order(created_at: :desc)
    elsif current_user.is_human_capital?
      @conversation = Conversation.find(params[:conversation_id])
      @message =  @conversation.messages.order(created_at: :asc)
    end
  end



  # GET /messages/1
  # GET /messages/1.json
  def show
  end

  # GET /messages/new
  def new
    @message = Message.new
  end

  # GET /messages/1/edit
  def edit
    @message = Message.find(params[:id])
    respond_to do |format|
      format.html do
        flash[:warning] = "Template missing"
        redirect_to root_path
      end
      format.js { render template: 'messages/modal.js.erb' }
    end
  end



  def update
    @message = Message.find(params[:id])

    if @message.update_attributes(message_params)
      flash[:success] = "User updated"
      redirect_to answer_chat_path(id:@massge.conversation.id)
    else
      render 'edit'
    end
  end

  def partial_conver
    if params[:conversation_id].present?
      @conversation = Conversation.where(id: params[:conversation_id]).where(user_id: current_user.id).last
    else
      @conversation = current_user.conversations.last
    end
    render :partial => "messages/list_conversation"
  end

  # POST /messages
  # POST /messages.json
  def create
    @message = Message.new(message_params)
    respond_to do |format|
      if @message.save
        format.html {
          redirect_to @message, notice: 'Message was successfully posted.'

        }
        format.json { render :show, status: :created, location: @message }
        if User.find(@message.user_id).is_motivator?
          NotifMailer.replymotivator(@message.conversation.user).deliver
          Conversation.find(@message.conversation_id).update_attribute(:status,"motivator-reply")
          unless NotificationDatum.where(id: @message.conversation.user.notif_id).count == 0
            @notification_data = NotificationDatum.find(@message.conversation.user.notif_id)
            Webpush.payload_send(endpoint: @notification_data.endpoint,
              message: "Hai, #{@message.conversation.user.full_name}. Ada balasan dari motivator" ,
              url: "/dashboard",
              p256dh: @notification_data.p256dh_key,
              auth: @notification_data.auth_key,
              ttl: 12 * 60 * 60,
              vapid: {
                subject: 'mailto:aulya@gmail.com',
                expiration: 12 * 60 * 60,
                public_key: $vapid_public,
                private_key: $vapid_private,
                })
              end

            else
              x = Conversation.find(@message.conversation_id)
              unless x.motivator_id == nil
                NotifMailer.replyclient(@message.conversation.motivator).deliver
                unless NotificationDatum.where(id: @message.conversation.motivator.notif_id).count == 0
                @notification_data = NotificationDatum.find(@message.conversation.motivator.notif_id)
                  Webpush.payload_send(endpoint: @notification_data.endpoint,
                    message: "Hai, motivator. Ada balasan dari client" ,
                    url: "/dashboard",
                    p256dh: @notification_data.p256dh_key,
                    auth: @notification_data.auth_key,
                    ttl: 12 * 60 * 60,
                    vapid: {
                      subject: 'mailto:aulya@gmail.com',
                      expiration: 12 * 60 * 60,
                      public_key: $vapid_public,
                      private_key: $vapid_private,
                      })
                    end
                  end
                  Conversation.find(@message.conversation_id).update_attribute(:status,"client-reply")
                end
              else
                format.html { render :new }
                format.json { render json: @message.errors, status: :unprocessable_entity }
              end
            end
          end
        end

        # PATCH/PUT /messages/1
        # PATCH/PUT /messages/1.json


        # DELETE /messages/1
        # DELETE /messages/1.json
        def destroy
          @message.destroy
          respond_to do |format|
            format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
            format.json { head :no_content }

          end
        end



        private
        # Use callbacks to share common setup or constraints between actions.
        def set_message
          @message = Message.find(params[:id])
        end



        def set_conversation
          @conversation = Conversation.find(params[:conversation_id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def message_params
          params.require(:message).permit(:content, :user_id, :time, :conversation_id, :content_reply)
        end
