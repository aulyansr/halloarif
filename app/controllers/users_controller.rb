class UsersController < ApplicationController
  before_action :authenticate_user!
  layout :determine_layout
  before_action :set_user, only: [:show, :edit, :update, :destroy]
    # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    if current_user.is_client?
      if @user.referral_code.nil? || @user.referral_code.blank?
        @user.update_attribute(:referral_code, [@user.full_name.gsub(/[^0-9A-Za-z]/, '').first(7).to_s, DateTime.now.to_i.to_s.last(4).to_i ].join.to_s)
      end
      unless current_user.identity_complete?
        redirect_to after_register_path(:add_identity)
      end
    end
    # http://localhost:3000/register?referral_code=aulya4750
    @message_box = MessageBox.new

    if current_user.is_human_capital?
      @message_boxes = MessageBox.where(motivator_id: @user.id)
    end
  end

  def message_box
    render :partial => "users/message_box"
  end

  # GET /users/new
  def new
    @user = User.new
    @user.detail_motivators.build
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.edit_as_admin?
    respond_to do |format|
      if @user.save
        @user.confirm
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if @user.is_motivator?
       @user.skip_reconfirmation!
    end
    respond_to do |format|
      if params[:edit_password].present?
        if @user_password.update_with_password(user_params_password)
          # Sign in the user by passing validation in case their password changed
          sign_in @user_password, :bypass => true
          format.html { redirect_to @user, notice: 'Password was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit, alert: 'Uups , maybe you miss something' }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end

      else
        if @user.update(user_params)
          # Sign in the user by passing validation in case their password changed
          if current_user.is_client?
            format.html { redirect_to dashboard_index_path, notice: 'User was successfully updated.' }
          else
            format.html { redirect_to @user, notice: 'User was successfully updated.' }
          end
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit, alert: 'Uups , maybe you miss something' }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end

      end

    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
      @user_password = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:motivator_code, :active_promo, :motivator_type, :email, :full_name, :phone_number,:role, :password, :password_confirmation,:range_age, :gender, :religion, :marital_status, :occupation, :province_id, :city_id, :register_source, :detail_motivators_attributes => [:nama_panjang, :nama_panggilan, :email_pribadi, :telpon_rumah, :handphone, :alamat_rumah, :region_ktp, :tinggal_sekarang, :region_sekarang, :no_ktp, :tanggal_lahir, :tempat_lahir, :no_kartu_keluarga, :pendidikan_terakhir, :no_ijazah, :pekerjaan, :nama_perusahaan, :keahlian_khusus, :nama_bank,:range_age, :gender, :religion, :marital_status, :occupation, :province_id, :city_id, :register_source, :nama_pemilik_rekening, :no_rekening, :jenis_rekening, :emergency_name_1, :emergency_phone_1, :emergency_name_2, :emergency_phone_2, :problem_category_id])
    end

    def user_params_password
   params.require(:user).permit(:password, :password_confirmation, :current_password )
 end

    def determine_layout
      current_user.is_client? ? "application" : "admin"
    end
end
