class PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  # GET /payments
  # GET /payments.json
  def index
    @payments = Payment.all
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  # GET /payments/new
  def new
    @payment = Payment.new
  end

  # GET /payments/1/edit
  def edit
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to @payment, notice: 'Payment was successfully created.' }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html {
        c = Conversation.new
        c.user_id =  current_user.id
        c.package_id = @payment.package_id
        c.status = "init"
        c.save(validate: false)
        redirect_to before_conversation_path(:question_1), notice: 'Paket Berhasil di Aktifkan !' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def validate_promo_code
    if Promo.where(promo_type: "promo-code").where(code: params[:promo_code].upcase).exists?
      promo = Promo.where(code: params[:promo_code].upcase).last
      if promo.end_date < Date.today
        redirect_back(fallback_location: root_path, alert: "Promo telah berakhir")
      else
        payment = Payment.find(params[:payment_id])
        payment.update_attribute(:promo_code, promo.code)
        if promo.discount.to_i == 100
          c = Conversation.new
          c.user_id =  current_user.id
          c.package_id = payment.package_id
          c.status = "init"
          c.save(validate: false)
          redirect_to before_conversation_path(:question_1), notice: 'Paket Berhasil di Aktifkan !'
        else
          redirect_back(fallback_location: root_path, notice: "Kode Promo berhasil")
        end
      end
      else
        redirect_back(fallback_location: root_path, alert: "Kode Promo Tidak Ditemukan")
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def init_payment
    c = Payment.new
    c.user_id =  current_user.id
    c.package_id = params[:packages_id]
    c.status = "init"
    c.save(validate: false)
    # redirect_to before_conversation_path(:question_1)
    redirect_to payment_detail_path(payment: c.id)
  end

  def notify
    Payment.find(params["order_id"]).update_attribute(
      :status, params["transaction_status"]
    )

    x = Payment.find(params["order_id"])
    if x.status == "settlement"
      c = Conversation.new
      c.user_id =  x.user_id
      c.package_id = x.package_id
      c.status = "init"
      c.save(validate: false)
    end


    # must return status 200
    render text: "ACK"
  end

  def payment_success

  end

  def payment_pending

  end

  def payment_error

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:user_id, :package_id, :promo_id, :status, :promo_code)
    end
end
