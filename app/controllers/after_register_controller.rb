class AfterRegisterController < ApplicationController
  include Wicked::Wizard
  steps :add_identity, :add_address

  def show
    @user = current_user
    render_wizard
  end

  def update
    @user = current_user
    @user.update_attributes(user_params)
    render_wizard @user
  end
  private

  def finish_wizard_path
    flash[:notice] = 'Akun Kamu Berhasil di buat'
    user_path(current_user)
  end

  def user_params
    params.require(:user).permit(:range_age, :gender, :religion, :marital_status, :occupation, :province_id, :city_id, :register_source )
  end

end
