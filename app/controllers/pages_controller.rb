class PagesController < ApplicationController
  def index

    @video = Video.all
  end

  def about
  end

  def contact
  end

  def payment
    @payment = Payment.find(params[:payment].to_i)
    response = Veritrans.create_widget_token(
      transaction_details: {
        order_id: @payment.id,
        gross_amount: @payment.package.price
      }
    )

    @snap_token = response.token

  end

  def list_package
    @packages = Package.all
  end

  def history
    @message_boxes = MessageBox.where(user_id: current_user.id).where.not(message: nil).where.not(question_1: nil).where.not(question_2: nil)
    @conversations = Conversation.where(user_id: current_user.id).where.not(question_1: nil).where.not(question_2: nil)
  end

  def history_mtv
    @message_boxes = MessageBox.where(motivator_id: current_user.id).where.not(message: nil).where.not(question_1: nil).where.not(message: nil).where.not(question_2: nil)
    @conversations = Conversation.where(motivator_id: current_user.id).where.not(question_1: nil).where.not(question_2: nil)
  end

  def user_agrement

  end

  def intro
    #code
  end

  def conduct
    #code
  end

  def questioner
    #code
  end

  def list_box
    if current_user.is_client?
      @box = MessageBox.where(user_id: current_user.id).where(status: ["init","finish"])
    else
      @box = MessageBox.where(motivator_id: current_user.id).where.not(status: ["finish", "rated"])
    end
  end

  def list_chat
    if current_user.is_client?
      @box = Conversation.where(user_id: current_user.id).where(status: ["init","finish"])
    else
      @box = Conversation.where(motivator_id: current_user.id).where.not(status: ["finish", "rated"])
    end
  end

  def answer
    @conversation = MessageBox.find(params[:id])
  end

  def answer_chat
    @conversation = Conversation.find(params[:id])
    @messagex = Message.new
  end

  def rate_box
    @conversation = MessageBox.find(params[:mvt])
  end

  def allstory
   @articles = Article.where(status: "publish").order(created_at: :desc).page(params[:page]).per(6)
  end

  def partial_conver
    @conversation = Conversation.where(id: params[:id]).last
    render :partial => "pages/list_conversation"
  end

  def mtv_info
    @schedule = Schedule.new
  end

  def sendPush
    notif_data = NotificationDatum.create(endpoint: params[:subscription][:endpoint],
      p256dh_key: params[:subscription][:keys][:p256dh],
      auth_key: params[:subscription][:keys][:auth])
      updatex = current_user.update_attributes(auth_key: params[:subscription][:keys][:auth], :notif_id => notif_data.id)
      user = current_user
      sendPayload(user)
      render body: nil
    end

    def sendPayload(user)
      @message = get_message(user.id)
      if user.notif_id.present?
        @notification_data = NotificationDatum.find(user.notif_id)
        Webpush.payload_send(endpoint: @notification_data.endpoint,
          message: @message,
          p256dh: @notification_data.p256dh_key,
          auth: @notification_data.auth_key,
          ttl: 24 * 60 * 60,
          vapid: {
            subject: 'mailto:admin@commercialview.com.au',
            public_key: "BH7BwHWvBIzJWoN7pTYB9BXpwZHSKbV3F6cQ2EnDz3brpRAUyxMDv6GKBcXodzPEBy6bpo7QkWVdSjQDvTq3kJ8=",
            private_key: "Rx92kgrI4NWd4_VWlWnsSw3dpCv0nI-kNX9PW2HfX2A="
          }
        )
        puts "success"
      else
        puts "failed"
      end
    end

    def get_message(name)
      "Hello World"
    end

  end
