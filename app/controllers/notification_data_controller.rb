class NotificationDataController < ApplicationController
  before_action :set_notification_datum, only: [:show, :edit, :update, :destroy]

  # GET /notification_data
  # GET /notification_data.json
  def index
    @notification_data = NotificationDatum.all
  end

  # GET /notification_data/1
  # GET /notification_data/1.json
  def show
  end

  # GET /notification_data/new
  def new
    @notification_datum = NotificationDatum.new
  end

  # GET /notification_data/1/edit
  def edit
  end

  # POST /notification_data
  # POST /notification_data.json
  def create
    @notification_datum = NotificationDatum.new(notification_datum_params)

    respond_to do |format|
      if @notification_datum.save
        format.html { redirect_to @notification_datum, notice: 'Notification datum was successfully created.' }
        format.json { render :show, status: :created, location: @notification_datum }
      else
        format.html { render :new }
        format.json { render json: @notification_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notification_data/1
  # PATCH/PUT /notification_data/1.json
  def update
    respond_to do |format|
      if @notification_datum.update(notification_datum_params)
        format.html { redirect_to @notification_datum, notice: 'Notification datum was successfully updated.' }
        format.json { render :show, status: :ok, location: @notification_datum }
      else
        format.html { render :edit }
        format.json { render json: @notification_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notification_data/1
  # DELETE /notification_data/1.json
  def destroy
    @notification_datum.destroy
    respond_to do |format|
      format.html { redirect_to notification_data_url, notice: 'Notification datum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification_datum
      @notification_datum = NotificationDatum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_datum_params
      params.require(:notification_datum).permit(:endpoint, :p256dh_key, :auth_key)
    end
end
