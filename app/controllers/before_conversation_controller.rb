class BeforeConversationController < ApplicationController
  include Wicked::Wizard
  steps :question_1, :category, :question_2

  def show
    @conversation = Conversation.where(user_id: current_user).last
    render_wizard
  end

  def update
    @conversation = Conversation.where(user_id: current_user).last
    @conversation.update_attributes(conversation_params)
    render_wizard @conversation
  end
  private

  def finish_wizard_path
    messages_path(cht:"new")
  end

  def conversation_params
    params.require(:conversation).permit(:question_1,:question_2, :problem_category_id)
  end

end
