class DashboardController < ApplicationController
  before_action :authenticate_user!
  layout :determine_layout
  def index
    if current_user.is_admin? || current_user.is_human_capital? || current_user.is_motivator? || current_user.is_finance?
      @all_client = User.client
      @new_client = User.client.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @message_box = MessageBox.where.not(message: "")
      @message_box_unread = MessageBox.where(status: "init")
      @message_box_process = MessageBox.where(status: "motivator-selected").where.not(problem_category_id: "").where.not(message: "").where.not(question_1: "").where.not(message: "").where.not(question_2: "")
      @message_box_process = MessageBox.where(status: "finish").where.not(problem_category_id: "").where.not(message: "").where.not(question_1: "").where.not(message: "").where.not(question_2: "")
      @user_from_mkt =  @new_client.where(signup_referral_code: ["MKT-01", "MKT-02","MKT-03" "MKT-04", "MKT-05", "MKT-06"]).count
      @user_from_dkt = @new_client.where(signup_referral_code: ["DKT-01", "DKT-02","DKT-03" "DKT-04", "DKT-05", "DKT-06"]).count
      @user_direct = @new_client.where.not(signup_referral_code: ["DKT-01", "DKT-02","DKT-03" "DKT-04", "DKT-05", "DKT-06","MKT-01", "MKT-02","MKT-03" "MKT-04", "MKT-05", "MKT-06"]).count
      @conversation = Conversation.where.not(problem_category_id: nil).where.not(status: ["motivator-selected","motivator-reply","finish"])
      @users = User.motivator
    end
    if current_user.is_motivator?
      @message_box_unread = MessageBox.where(status:'motivator-selected').where(motivator_id: current_user.id)
      @all_conversation = Conversation.where(motivator_id: current_user.id)
      @conversation = Conversation.where(motivator_id: current_user.id).where.not(status: 'finish')

      @notif_msg_box = MessageBox.where(motivator_id: current_user.id).where.not(status: ["finish", "rated"]).count
      @notif_lv_chat = Conversation.where(motivator_id: current_user.id).where.not(status: ["finish", "rated"]).count
    end
    if current_user.is_client?
      unless current_user.identity_complete?
        redirect_to questioner_path
      end
    end

  end
    def partial_conver
      if current_user.is_admin? || current_user.is_human_capital? || current_user.is_motivator? || current_user.is_finance?
        @all_client = User.client
        @new_client = User.client.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
        @message_box = MessageBox.where.not(message: "").where.not(problem_category_id: "").where.not(question_1: "").where.not(message: "").where.not(question_2: "")
        @message_box_unread = MessageBox.where(status: "init").where.not(problem_category_id:"").where.not(message: "").where.not(question_1: "").where.not(message: "").where.not(question_2: "")
        @message_box_process = MessageBox.where(status: "motivator-selected").where.not(problem_category_id: "").where.not(message: "").where.not(question_1: "").where.not(message: "").where.not(question_2: "")
        @message_box_process = MessageBox.where(status: "finish").where.not(problem_category_id: "").where.not(message: "").where.not(question_1: "").where.not(message: "").where.not(question_2: "")
        @user_from_mkt =  @new_client.where(signup_referral_code: ["MKT-01", "MKT-02","MKT-03" "MKT-04", "MKT-05", "MKT-06"]).count
        @user_from_dkt = @new_client.where(signup_referral_code: ["DKT-01", "DKT-02","DKT-03" "DKT-04", "DKT-05", "DKT-06"]).count
        @user_direct = @new_client.where.not(signup_referral_code: ["DKT-01", "DKT-02","DKT-03" "DKT-04", "DKT-05", "DKT-06","MKT-01", "MKT-02","MKT-03" "MKT-04", "MKT-05", "MKT-06"]).count
      @conversation = Conversation.where.not(problem_category_id: nil).where.not(status: ["motivator-selected","motivator-reply","finish","rated"])
      end
      if current_user.is_motivator?
        @message_box_unread = MessageBox.where(status:'motivator-selected').where(motivator_id: current_user.id)
        @all_conversation = Conversation.where(motivator_id: current_user.id)
        @conversation = Conversation.where(motivator_id: current_user.id).where.not(status: 'finish')
      end
      render :partial => "dashboard/list_conversation"
    end
    def partial_msgbox

      render :partial => "dashboard/list_msgbox"
    end

    def partial_notif

      render :partial => "dashboard/list_notif"
    end

    def sendPush
		notif_data = NotificationDatum.create(endpoint: params[:subscription][:endpoint],
		                        p256dh_key: params[:subscription][:keys][:p256dh],
		                        auth_key: params[:subscription][:keys][:auth])

		user = current_user.update_attributes(auth_key: params[:subscription][:keys][:auth], :notif_id => notif_data.id)
    user = current_user
		sendPayload(user)
		render body: ""
	end



    private
    def determine_layout
      if current_user.is_client? || current_user.is_motivator?
        "application"
      else
        "admin"
      end
    end
end
