class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :prepare_meta_tags
  layout :layout_by_resource
  include Response
  include Authenticate
  include SerializableResource
  PushNotificationError = Class.new(StandardError)

  $vapid_private =  "6tKH8lmEGWpYZjjNSBMYAwaK46R2UPd8gSDNTTdJ7rs="
  $vapid_public = "BI8zYHMWsy-ETJe8jtVSAXbnXK5SvFjZ3Sv9E-58SRlFgfj4BFZFn7pkO5LiVAdbcShfKqkfV4ZLWaz9uNXSsGY="

  def prepare_meta_tags(options={})
    site_name   = "HALLO ARIF"
    title       = "LAYANAN CURHAT ONLINE DAN MOTIVASI MASA KINI"
    description = "Layanan curhat online dan motivasi masa kini, tempat berbagi keluh kesah dengan aman dan nyaman bersama motivator berpengalaman"
    image       = "https://www.halloarif.id/assets/Box%20Uneg-Uneg.png"
    current_url = "https://www.halloarif.id/"

    # Let's prepare a nice set of defaults
    defaults = {
      reverse: true,
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      keywords:    %w[curhat motivasi motivator ],
      twitter: {
        site_name: site_name,
        site: '@halloarif.id',
        creator: '@halloarif.id',
        card: 'summary_large_image',
        description: description,
        title: title,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options

  end

  protected
  def layout_by_resource
    if devise_controller?
      "login"
    else
      "application"
    end
  end

  def after_sign_in_path_for(resource)
    if  current_user.is_client?
      if current_user.identity_complete?
        dashboard_index_path
      else
        intro_path
      end
    elsif current_user.is_motivator?
      conduct_path
    else
      dashboard_index_path
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:full_name,:phone_number,:role, :signup_referral_code, :active_promo])
  end
end
