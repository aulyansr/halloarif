class Api::V1::UsersController  < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_with_token!


  def show
    user = the_user
  end

  def update_identity
    #json_response "Show book successfully", true, {user: User.find(the_user.id)}, :ok
     user =  the_user
     if user.update user_params
        json_response "Profile Berhasil di Update", true, {user: user}, :ok
     else
        json_response user.errors, false, {}, :unprocessable_entity
     end
  end

  private
  def user_params
     params.require(:user).permit :range_age, :occupation, :province_id, :city_id, :gender, :marital_status
  end
end
