class Api::V1::RegistrationsController < Devise::RegistrationsController
  skip_before_action :verify_authenticity_token
  # sign up
  def create
    user = User.new user_params
    user.role = "CLT"
    if user.save
      json_response "Registrasi Berhasil, Cek Email untuk Aktivasi Akun Kamu", true, {user: user}, :ok
    else
      json_response user.errors, false, {}, :unprocessable_entity
    end
  end

  private
  def user_params
    params.require(:user).permit(:email,:full_name,:phone_number,:signup_referral_code, :password, :password_confirmation)
  end


end
