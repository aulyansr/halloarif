class Api::V1::MessageBoxesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_with_token!
  before_action :load_message_box, only: [:show, :create_rating]

  def index
    message_boxes =  the_user.messeage_boxs_client
    json_response "History Semua Box Uneg Uneg", true, {message_boxes: message_boxes}, :ok
  end

  def create
    if the_user.identity_complete?
      if the_user.messeage_boxs_client.last.message_reply == nil
        json_response "Motivator Belum Menjawab Curhatan kamu sebelumnnya", false, {}, :unprocessable_entity
      else
        message_box = MessageBox.new message_box_params
        message_box.user_id = the_user.id
        message_box.status = "init"
        if message_box.save
            json_response "Curhatan Kamu Berhasil Di Kirim. Tunggu Balasan Dari Motivator", true, {message_box: message_box}, :ok
          else
            json_response message_box.errors, false, {}, :unprocessable_entity
        end

      end
    else
      json_response "Harap Lengkapi Identitas", false, {}, :unprocessable_entity
    end
  end

  def show
    if the_user.messeage_boxs_client.map(&:id).include?(@message_box.id)
      json_response "Curhatan Kamu", true, {message_box: @message_box}, :ok
    else
      json_response "Tidak Ditemukan", false, {}, :unprocessable_entity
    end
  end

  def create_rating
    if the_user.messeage_boxs_client.map(&:id).include?(@message_box.id)
      if @message_box.update rating_params
         json_response "Terima Kasih", true, {message_box: @message_box}, :ok
      else
         json_response @message_box.errors, false, {}, :unprocessable_entity
      end
    else

    end
  end

  private
  def load_message_box
    @message_box = MessageBox.find_by id: params[:id]
    unless @message_box.present?
      json_response "Curhatan Kamu Tidak Ditemukan !", false, {}, :not_found
    end
  end

  def message_box_params
    params.require(:message_box).permit  :message, :problem_category_id, :question_1, :question_2
  end

  def rating_params
    params.require(:message_box).permit(:rating, :reason)
  end


end
