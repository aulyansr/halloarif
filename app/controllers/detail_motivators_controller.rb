class DetailMotivatorsController < ApplicationController
  before_action :set_detail_motivator, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'admin'
  # GET /detail_motivators
  # GET /detail_motivators.json
  def index
    @detail_motivators = DetailMotivator.all
  end

  # GET /detail_motivators/1
  # GET /detail_motivators/1.json
  def show
  end

  # GET /detail_motivators/new
  def new
    @detail_motivator = DetailMotivator.new
  end

  # GET /detail_motivators/1/edit
  def edit
  end

  # POST /detail_motivators
  # POST /detail_motivators.json
  def create
    @detail_motivator = DetailMotivator.new(detail_motivator_params)

    respond_to do |format|
      if @detail_motivator.save
        format.html { redirect_to @detail_motivator, notice: 'Detail motivator was successfully created.' }
        format.json { render :show, status: :created, location: @detail_motivator }
      else
        format.html { render :new }
        format.json { render json: @detail_motivator.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detail_motivators/1
  # PATCH/PUT /detail_motivators/1.json
  def update
    respond_to do |format|
      if @detail_motivator.update(detail_motivator_params)
        format.html { redirect_to user_path(@detail_motivator.user_id), notice: 'Detail motivator was successfully updated.' }
        format.json { render :show, status: :ok, location: @detail_motivator }
      else
        format.html { render :edit }
        format.json { render json: @detail_motivator.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detail_motivators/1
  # DELETE /detail_motivators/1.json
  def destroy
    @detail_motivator.destroy
    respond_to do |format|
      format.html { redirect_to detail_motivators_url, notice: 'Detail motivator was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detail_motivator
      @detail_motivator = DetailMotivator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detail_motivator_params
      params.require(:detail_motivator).permit(:nama_panjang, :nama_panggilan, :email_pribadi, :telpon_rumah, :handphone, :alamat_rumah, :region_ktp, :tinggal_sekarang, :region_sekarang, :no_ktp, :tanggal_lahir, :tempat_lahir, :no_kartu_keluarga, :pendidikan_terakhir, :no_ijazah, :pekerjaan, :nama_perusahaan, :keahlian_khusus, :nama_bank, :nama_pemilik_rekening, :no_rekening, :jenis_rekening, :emergency_name_1, :emergency_phone_1, :emergency_name_2, :emergency_phone_2, :problem_category_id)
    end
end
