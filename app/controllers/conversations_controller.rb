class ConversationsController < ApplicationController
  before_action :set_conversation, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'admin'
  # GET /conversations
  # GET /conversations.json
  def index
    if current_user.is_motivator?
      @conversations = Conversation.where(motivator_id:current_user.id)
    else
      @conversations = Conversation.all
    end
  end

  # GET /conversations/1
  # GET /conversations/1.json
  def show
    @message = Message.new
  end

  def finish
    @conversation = Conversation.find(params[:id])
    @conversation.update_attribute(:status, "finish")
    if current_user.is_client?
      redirect_to messages_path(conversation_id: @conversation.id), notice: 'Paket Berhasil telah Berakhir !'
    else
      redirect_to dashboard_path(conversation_id: @conversation.id), notice: 'Paket Berhasil telah Berakhir !'
    end
  end

  # GET /conversations/new
  def new
    @conversation = Conversation.new
  end

  # GET /conversations/1/edit
  def edit
  end

  # POST /conversations
  # POST /conversations.json
  def create
    @conversation = Conversation.new(conversation_params)

    respond_to do |format|
      if @conversation.save
        format.html { redirect_to @conversation, notice: 'Conversation was successfully created.' }
        format.json { render :show, status: :created, location: @conversation }
      else
        format.html { render :new }
        format.json { render json: @conversation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /conversations/1
  # PATCH/PUT /conversations/1.json
  def update
    respond_to do |format|
      if @conversation.update(conversation_params)
        format.html { redirect_to dashboard_index_path(chtmtv:@conversation.motivator_id) }
        format.json { }
      else
        format.html { render :edit }
        format.json { render json: @conversation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conversations/1
  # DELETE /conversations/1.json
  def destroy
    @conversation.destroy
    respond_to do |format|
      format.html { redirect_to conversations_url, notice: 'Conversation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def init_conversation
    c = Conversation.new
    c.user_id =  current_user.id
    # c.package_id = params[:packages_id]
    c.status = "init"
    c.save(validate: false)
    NotifMailer.newchat.deliver
    redirect_to before_conversation_path(:question_1), notice: 'Paket Berhasil di Aktifkan !'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_conversation
      @conversation = Conversation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def conversation_params
      params.require(:conversation).permit(:user_id, :motivator_id, :package_id, :question_1, :question_2, :problem_category_id, :status, :rating, :reason)
    end
end
