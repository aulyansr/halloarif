module ApplicationHelper
  def slider_active(i)
    case i
    when 0 then "active"
    end
  end

  def gotoid(x)
    if x == "index" && controller_name =="pages"
      ""
    else
      "/"
    end
  end
  def custom_id_box(x)
      "010#{x.problem_category_id}#{x.created_at.strftime('%d%m%H%M')}"
  end
  def custom_id_chat(x)
    "020#{x.problem_category_id}#{x.created_at.strftime('%d%m%H%M')}"
  end
end
