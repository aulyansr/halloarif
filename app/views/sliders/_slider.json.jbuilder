json.extract! slider, :id, :name, :status, :description, :created_at, :updated_at
json.url slider_url(slider, format: :json)
