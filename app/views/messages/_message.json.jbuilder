json.id @message.id
json.content @message.content
json.full_name @message.user.full_name
json.motivator_code @message.user.motivator_code
json.url message_url(@message, format: :json)
