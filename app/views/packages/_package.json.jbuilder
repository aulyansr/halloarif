json.extract! package, :id, :name, :description, :duration, :price, :created_at, :updated_at
json.url package_url(package, format: :json)
