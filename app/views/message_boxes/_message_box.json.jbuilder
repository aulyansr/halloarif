json.extract! message_box, :id, :user_id, :motivator_id, :status, :message, :message_reply, :problem_category_id, :created_at, :updated_at
json.url message_box_url(message_box, format: :json)
