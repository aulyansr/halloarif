json.extract! notification_datum, :id, :endpoint, :p256dh_key, :auth_key, :created_at, :updated_at
json.url notification_datum_url(notification_datum, format: :json)
