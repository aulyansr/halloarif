json.data(Regency.all) do |x|
  json.province_code x.province_code
  json.code x.code
  json.name x.name
end
