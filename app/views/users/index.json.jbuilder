json.data(@users) do |x|
  json.full_name x.detail_motivators.last.nama_panjang
  json.motivator_code x.motivator_code
  json.motivator_type x.motivator_type
  json.email  x.email
end
