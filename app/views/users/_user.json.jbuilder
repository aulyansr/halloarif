json.extract! user, :id, :full_name, :phone_number, :status, :role, :birth_date, :birt_place, :gender, :religion, :profession, :nisn, :school, :city_id, :province_id, :image, :referral_code, :signup_referral_code, :created_at, :updated_at
json.url user_url(user, format: :json)
