json.extract! conversation, :id, :user_id, :motivator_id, :package_id, :question_1, :question_2, :problem_category_id, :created_at, :updated_at
json.url conversation_url(conversation, format: :json)
