json.extract! problem_category, :id, :name, :descriptions, :created_at, :updated_at
json.url problem_category_url(problem_category, format: :json)
