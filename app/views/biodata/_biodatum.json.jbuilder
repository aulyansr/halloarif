json.extract! biodatum, :id, :avatar, :gender, :age_range, :occupation, :story, :created_at, :updated_at
json.url biodatum_url(biodatum, format: :json)
