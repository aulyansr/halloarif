json.extract! payment, :id, :user_id, :package_id, :promo_id, :status, :created_at, :updated_at
json.url payment_url(payment, format: :json)
