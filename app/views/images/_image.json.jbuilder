json.extract! image, :id, :image, :caption, :target, :slider_id, :created_at, :updated_at
json.url image_url(image, format: :json)
