class Conversation < ApplicationRecord
  belongs_to :package, optional: true
  has_many :messages

  belongs_to :user
  belongs_to :motivator, :class_name => "User", :foreign_key => :motivator_id, optional: true
  belongs_to :problem_category, optional: true
end
