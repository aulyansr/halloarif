class Article < ApplicationRecord
  # belongs_to :article_category
  extend FriendlyId
  friendly_id :title

  mount_uploader :image, ImageUploader
end
