class Schedule < ApplicationRecord
  Type = {senin: 'monday', selasa: 'tuesday', rabu: 'wednesday', kamis: 'thursday', jumat: 'friday', sabtu: 'saturday', minggu: 'sunday'}
  belongs_to :motivator, :class_name => "User", :foreign_key => :motivator_id, optional: true
end
