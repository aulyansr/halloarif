class ProblemCategory < ApplicationRecord
  Type = ['PERCINTAAN', 'KEUANGAN', 'KELUARGA', 'PERTEMANAN', 'PERMASALAHAN DIRI', 'STUDI KARIR']
  has_many :message_boxes
  has_many :conversations
end
