class ArticleCategory < ApplicationRecord
  extend FriendlyId
  friendly_id :name
end
