class Message < ApplicationRecord
  after_create :notify_pusher, on: :create
  belongs_to :conversation
  belongs_to :user


  def notify_pusher
    Pusher.trigger('message', 'new', {"conversation_id":self.conversation_id, "id": self.id, "full_name": self.user.full_name.nil? ? " ":  self.user.full_name, "content": self.content, "motivator_code": self.user.motivator_code.nil? ? " ":  self.user.motivator_code, "pos": self.user.is_motivator? ? "ml-auto" : " ","date": self.created_at.strftime("%e %B %Y  %T ")}.as_json)
  end

end
