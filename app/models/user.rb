class User < ApplicationRecord
  acts_as_token_authenticatable
  # Include default devise modules. Others available are:
  devise :database_authenticatable,:confirmable,  :registerable,:recoverable, :timeoutable , :rememberable, :validatable, :lockable, :trackable and :omniauthable

  validates :email, uniqueness: true
  validates :phone_number, uniqueness: true, on: :create, unless: :edit_as_admin?


  ROLES = {super_admin: 'SPADM', client: 'CLT', motivator: 'MTV' , human_capital: 'HMCPT', finance: 'FNC'}
  TYPE = ['MOTIVATOR EKSKLUSIF', 'Motivator Inti', 'Motivator Jakarta' ,'Motivator Metro']
  SOURCE = ['Social Media', 'Teman', 'Team Marketing']
  scope :super_admin, -> {where(role: ROLES[:super_admin])}
  scope :client, -> {where(role: ROLES[:client])}
  scope :motivator, -> {where(role: ROLES[:motivator])}
  scope :human_capital, -> {where(role: ROLES[:human_capital])}
  scope :finance, -> {where(role: ROLES[:finance])}

  has_many :messeage_boxs_motivator, :class_name => "MessageBox", :foreign_key => :motivator_id
  has_many :messeage_boxs_client, :class_name => "MessageBox", :foreign_key => :user_id
  has_many :conversations
  has_many :conversations_motivator, :class_name => "Conversation", :foreign_key => :motivator_id
  has_many :schedules, :class_name => "Schedule", :foreign_key => :motivator_id

  has_many :detail_motivators
  accepts_nested_attributes_for :detail_motivators
  after_create :notify_pusher # add this line


  OCCUPATION = ["swasta", "pegawai negeri / PNS",  "ABRI"]
  KEAHLIANKHUSUS = ["dokter umum", "bedah",  "montir"]
  JENISREKENING = ["giro", "tabungan",  "rupiah", "dolar"]


  def edit_as_admin?
    @edit_as_admin = true
  end

  def is_admin?
    self.role == ROLES[:super_admin]
  end

  def is_client?
    self.role == ROLES[:client]
  end

  def is_motivator?
    self.role == ROLES[:motivator]
  end

  def is_human_capital?
    self.role == ROLES[:human_capital]
  end

  def is_finance?
    self.role == ROLES[:finance]
  end

  def identity_complete?
    self.range_age != nil and
    self.gender != nil and
    self.marital_status != nil and
    self.occupation != nil and
    self.province_id != nil
  end


  def notify_pusher # add this method
    Pusher.trigger('activity', 'login', self.as_json)
  end

  def as_json(options={}) # add this method
    super(
      only: [:id, :email, :motivator_code, :full_name,:range_age, :occupation, :province_id, :city_id, :gender, :marital_status, :authentication_token]
    )
  end

  def generate_new_authentication_token
    token = User.generate_unique_secure_token
    update_attributes authentication_token: token
  end

end
