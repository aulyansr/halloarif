class Quote < ApplicationRecord
  extend FriendlyId
  friendly_id :name

  mount_uploader :image, ImageUploader
end
