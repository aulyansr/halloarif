require 'test_helper'

class NotificationDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @notification_datum = notification_data(:one)
  end

  test "should get index" do
    get notification_data_url
    assert_response :success
  end

  test "should get new" do
    get new_notification_datum_url
    assert_response :success
  end

  test "should create notification_datum" do
    assert_difference('NotificationDatum.count') do
      post notification_data_url, params: { notification_datum: { auth_key: @notification_datum.auth_key, endpoint: @notification_datum.endpoint, p256dh_key: @notification_datum.p256dh_key } }
    end

    assert_redirected_to notification_datum_url(NotificationDatum.last)
  end

  test "should show notification_datum" do
    get notification_datum_url(@notification_datum)
    assert_response :success
  end

  test "should get edit" do
    get edit_notification_datum_url(@notification_datum)
    assert_response :success
  end

  test "should update notification_datum" do
    patch notification_datum_url(@notification_datum), params: { notification_datum: { auth_key: @notification_datum.auth_key, endpoint: @notification_datum.endpoint, p256dh_key: @notification_datum.p256dh_key } }
    assert_redirected_to notification_datum_url(@notification_datum)
  end

  test "should destroy notification_datum" do
    assert_difference('NotificationDatum.count', -1) do
      delete notification_datum_url(@notification_datum)
    end

    assert_redirected_to notification_data_url
  end
end
