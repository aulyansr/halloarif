require 'test_helper'

class ProblemCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @problem_category = problem_categories(:one)
  end

  test "should get index" do
    get problem_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_problem_category_url
    assert_response :success
  end

  test "should create problem_category" do
    assert_difference('ProblemCategory.count') do
      post problem_categories_url, params: { problem_category: { descriptions: @problem_category.descriptions, name: @problem_category.name } }
    end

    assert_redirected_to problem_category_url(ProblemCategory.last)
  end

  test "should show problem_category" do
    get problem_category_url(@problem_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_problem_category_url(@problem_category)
    assert_response :success
  end

  test "should update problem_category" do
    patch problem_category_url(@problem_category), params: { problem_category: { descriptions: @problem_category.descriptions, name: @problem_category.name } }
    assert_redirected_to problem_category_url(@problem_category)
  end

  test "should destroy problem_category" do
    assert_difference('ProblemCategory.count', -1) do
      delete problem_category_url(@problem_category)
    end

    assert_redirected_to problem_categories_url
  end
end
