require 'test_helper'

class DetailMotivatorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @detail_motivator = detail_motivators(:one)
  end

  test "should get index" do
    get detail_motivators_url
    assert_response :success
  end

  test "should get new" do
    get new_detail_motivator_url
    assert_response :success
  end

  test "should create detail_motivator" do
    assert_difference('DetailMotivator.count') do
      post detail_motivators_url, params: { detail_motivator: { alamat_rumah: @detail_motivator.alamat_rumah, email_pribadi: @detail_motivator.email_pribadi, emergency_name_1: @detail_motivator.emergency_name_1, emergency_name_2: @detail_motivator.emergency_name_2, emergency_phone_1: @detail_motivator.emergency_phone_1, emergency_phone_2: @detail_motivator.emergency_phone_2, handphone: @detail_motivator.handphone, jenis_rekening: @detail_motivator.jenis_rekening, keahlian_khusus: @detail_motivator.keahlian_khusus, nama_bank: @detail_motivator.nama_bank, nama_panggilan: @detail_motivator.nama_panggilan, nama_panjang: @detail_motivator.nama_panjang, nama_pemilik_rekening: @detail_motivator.nama_pemilik_rekening, nama_perusahaan: @detail_motivator.nama_perusahaan, no_ijazah: @detail_motivator.no_ijazah, no_kartu_keluarga: @detail_motivator.no_kartu_keluarga, no_ktp: @detail_motivator.no_ktp, no_rekening: @detail_motivator.no_rekening, pekerjaan: @detail_motivator.pekerjaan, pendidikan_terakhir: @detail_motivator.pendidikan_terakhir, problem_category_id: @detail_motivator.problem_category_id, region_ktp: @detail_motivator.region_ktp, region_sekarang: @detail_motivator.region_sekarang, tanggal_lahir: @detail_motivator.tanggal_lahir, telpon_rumah: @detail_motivator.telpon_rumah, tempat_lahir: @detail_motivator.tempat_lahir, tinggal_sekarang: @detail_motivator.tinggal_sekarang } }
    end

    assert_redirected_to detail_motivator_url(DetailMotivator.last)
  end

  test "should show detail_motivator" do
    get detail_motivator_url(@detail_motivator)
    assert_response :success
  end

  test "should get edit" do
    get edit_detail_motivator_url(@detail_motivator)
    assert_response :success
  end

  test "should update detail_motivator" do
    patch detail_motivator_url(@detail_motivator), params: { detail_motivator: { alamat_rumah: @detail_motivator.alamat_rumah, email_pribadi: @detail_motivator.email_pribadi, emergency_name_1: @detail_motivator.emergency_name_1, emergency_name_2: @detail_motivator.emergency_name_2, emergency_phone_1: @detail_motivator.emergency_phone_1, emergency_phone_2: @detail_motivator.emergency_phone_2, handphone: @detail_motivator.handphone, jenis_rekening: @detail_motivator.jenis_rekening, keahlian_khusus: @detail_motivator.keahlian_khusus, nama_bank: @detail_motivator.nama_bank, nama_panggilan: @detail_motivator.nama_panggilan, nama_panjang: @detail_motivator.nama_panjang, nama_pemilik_rekening: @detail_motivator.nama_pemilik_rekening, nama_perusahaan: @detail_motivator.nama_perusahaan, no_ijazah: @detail_motivator.no_ijazah, no_kartu_keluarga: @detail_motivator.no_kartu_keluarga, no_ktp: @detail_motivator.no_ktp, no_rekening: @detail_motivator.no_rekening, pekerjaan: @detail_motivator.pekerjaan, pendidikan_terakhir: @detail_motivator.pendidikan_terakhir, problem_category_id: @detail_motivator.problem_category_id, region_ktp: @detail_motivator.region_ktp, region_sekarang: @detail_motivator.region_sekarang, tanggal_lahir: @detail_motivator.tanggal_lahir, telpon_rumah: @detail_motivator.telpon_rumah, tempat_lahir: @detail_motivator.tempat_lahir, tinggal_sekarang: @detail_motivator.tinggal_sekarang } }
    assert_redirected_to detail_motivator_url(@detail_motivator)
  end

  test "should destroy detail_motivator" do
    assert_difference('DetailMotivator.count', -1) do
      delete detail_motivator_url(@detail_motivator)
    end

    assert_redirected_to detail_motivators_url
  end
end
