require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get users_url
    assert_response :success
  end

  test "should get new" do
    get new_user_url
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { user: { birt_place: @user.birt_place, birth_date: @user.birth_date, city_id: @user.city_id, full_name: @user.full_name, gender: @user.gender, image: @user.image, nisn: @user.nisn, phone_number: @user.phone_number, profession: @user.profession, province_id: @user.province_id, referral_code: @user.referral_code, religion: @user.religion, role: @user.role, school: @user.school, signup_referral_code: @user.signup_referral_code, status: @user.status } }
    end

    assert_redirected_to user_url(User.last)
  end

  test "should show user" do
    get user_url(@user)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch user_url(@user), params: { user: { birt_place: @user.birt_place, birth_date: @user.birth_date, city_id: @user.city_id, full_name: @user.full_name, gender: @user.gender, image: @user.image, nisn: @user.nisn, phone_number: @user.phone_number, profession: @user.profession, province_id: @user.province_id, referral_code: @user.referral_code, religion: @user.religion, role: @user.role, school: @user.school, signup_referral_code: @user.signup_referral_code, status: @user.status } }
    assert_redirected_to user_url(@user)
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to users_url
  end
end
