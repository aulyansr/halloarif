class AddQuestionToMessageBox < ActiveRecord::Migration[5.1]
  def change
    add_column :message_boxes, :question_1, :string
    add_column :message_boxes, :question_2, :string
  end
end
