class AddCodeMotivatorToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :motivator_code, :string
    add_column :users, :motivator_type, :string
  end
end
