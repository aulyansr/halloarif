class AddPromoTypeToPromo < ActiveRecord::Migration[5.1]
  def change
    add_column :promos, :promo_type, :string
  end
end
