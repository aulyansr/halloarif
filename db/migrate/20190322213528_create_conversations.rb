class CreateConversations < ActiveRecord::Migration[5.1]
  def change
    create_table :conversations do |t|
      t.string :user_id
      t.string :motivator_id
      t.string :package_id
      t.string :question_1
      t.string :question_2
      t.string :problem_category_id

      t.timestamps
    end
  end
end
