class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.string :image
      t.string :caption
      t.string :target
      t.references :slider, foreign_key: true

      t.timestamps
    end
  end
end
