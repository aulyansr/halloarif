class ChangeFormatRatingMessageBox < ActiveRecord::Migration[5.1]
  def change
    change_column :message_boxes, :rating, :float
  end
end
