class AddSignStatusToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :is_signed_in, :boolean
  end
end
