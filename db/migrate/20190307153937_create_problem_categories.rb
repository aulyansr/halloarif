class CreateProblemCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :problem_categories do |t|
      t.string :name
      t.string :descriptions

      t.timestamps
    end
  end
end
