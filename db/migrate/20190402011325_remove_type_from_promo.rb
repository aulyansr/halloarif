class RemoveTypeFromPromo < ActiveRecord::Migration[5.1]
  def change
    remove_column :promos, :type, :string
  end
end
