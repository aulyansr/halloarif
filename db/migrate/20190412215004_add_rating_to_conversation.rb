class AddRatingToConversation < ActiveRecord::Migration[5.1]
  def change
    add_column :conversations, :rating, :float
    add_column :conversations, :reason, :text
  end
end
