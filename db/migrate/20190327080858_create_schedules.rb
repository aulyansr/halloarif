class CreateSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|
      t.string :date
      t.time :start
      t.time :end
      t.string :motivator_id

      t.timestamps
    end
  end
end
