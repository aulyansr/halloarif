class AddRatingToMessageBox < ActiveRecord::Migration[5.1]
  def change
    add_column :message_boxes, :rating, :integer
    add_column :message_boxes, :reason, :text
  end
end
