class AddEngageToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :engage, :integer
  end
end
