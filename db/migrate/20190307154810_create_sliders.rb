class CreateSliders < ActiveRecord::Migration[5.1]
  def change
    create_table :sliders do |t|
      t.string :name
      t.string :status
      t.string :description

      t.timestamps
    end
  end
end
