class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.string :user_id
      t.string :package_id
      t.string :promo_id
      t.string :status

      t.timestamps
    end
  end
end
