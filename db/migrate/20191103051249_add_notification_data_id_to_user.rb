class AddNotificationDataIdToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :notification_data_id, :string
  end
end
