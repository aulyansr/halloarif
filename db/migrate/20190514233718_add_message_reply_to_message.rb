class AddMessageReplyToMessage < ActiveRecord::Migration[5.1]
  def change
    add_column :messages, :content_reply, :text
  end
end
