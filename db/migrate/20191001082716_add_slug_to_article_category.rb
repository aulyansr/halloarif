class AddSlugToArticleCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :article_categories, :slug, :string
  end
end
