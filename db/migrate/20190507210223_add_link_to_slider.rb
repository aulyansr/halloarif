class AddLinkToSlider < ActiveRecord::Migration[5.1]
  def change
    add_column :sliders, :link, :string
  end
end
