class AddPromoAtributeToPromo < ActiveRecord::Migration[5.1]
  def change
    add_column :promos, :discount, :string
    add_column :promos, :limit, :string
    add_column :promos, :status, :string
  end
end
