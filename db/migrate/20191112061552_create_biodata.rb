class CreateBiodata < ActiveRecord::Migration[5.1]
  def change
    create_table :biodata do |t|
      t.string :avatar
      t.string :gender
      t.string :age_range
      t.string :occupation
      t.text :story

      t.timestamps
    end
  end
end
