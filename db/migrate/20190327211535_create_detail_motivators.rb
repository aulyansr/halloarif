class CreateDetailMotivators < ActiveRecord::Migration[5.1]
  def change
    create_table :detail_motivators do |t|
      t.string :nama_panjang
      t.string :nama_panggilan
      t.string :email_pribadi
      t.string :telpon_rumah
      t.string :handphone
      t.string :alamat_rumah
      t.string :region_ktp
      t.string :tinggal_sekarang
      t.string :region_sekarang
      t.string :no_ktp
      t.string :tanggal_lahir
      t.string :tempat_lahir
      t.string :no_kartu_keluarga
      t.string :pendidikan_terakhir
      t.string :no_ijazah
      t.string :pekerjaan
      t.string :nama_perusahaan
      t.string :keahlian_khusus
      t.string :nama_bank
      t.string :nama_pemilik_rekening
      t.string :no_rekening
      t.string :jenis_rekening
      t.string :emergency_name_1
      t.string :emergency_phone_1
      t.string :emergency_name_2
      t.string :emergency_phone_2
      t.string :problem_category_id

      t.timestamps
    end
  end
end
