class AddSlugToQuote < ActiveRecord::Migration[5.1]
  def change
    add_column :quotes, :slug, :string
  end
end
