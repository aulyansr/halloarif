class AddEngageToMessageBox < ActiveRecord::Migration[5.1]
  def change
    add_column :message_boxes, :engage, :integer
  end
end
