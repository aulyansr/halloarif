class AddDateToPromo < ActiveRecord::Migration[5.1]
  def change
    add_column :promos, :start_date, :date
    add_column :promos, :end_date, :date
  end
end
