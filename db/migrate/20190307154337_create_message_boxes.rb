class CreateMessageBoxes < ActiveRecord::Migration[5.1]
  def change
    create_table :message_boxes do |t|
      t.references :user, foreign_key: true
      t.string :motivator_id
      t.string :status
      t.text :message
      t.text :message_reply
      t.references :problem_category, foreign_key: true

      t.timestamps
    end
  end
end
