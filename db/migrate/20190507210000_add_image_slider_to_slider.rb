class AddImageSliderToSlider < ActiveRecord::Migration[5.1]
  def change
    add_column :sliders, :image_slider, :string
  end
end
