class AddPromoCodeToPayment < ActiveRecord::Migration[5.1]
  def change
    add_column :payments, :promo_code, :string
  end
end
