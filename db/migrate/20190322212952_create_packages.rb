class CreatePackages < ActiveRecord::Migration[5.1]
  def change
    create_table :packages do |t|
      t.string :name
      t.string :description
      t.string :duration
      t.string :price

      t.timestamps
    end
  end
end
