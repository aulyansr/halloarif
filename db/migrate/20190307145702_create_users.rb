class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :full_name
      t.string :phone_number
      t.string :range_age
      t.string :gender
      t.string :religion
      t.string :marital_status
      t.string :occupation
      t.integer :province_id
      t.string :referral_code
      t.string :signup_referral_code
      t.string :role
      t.string :speciality
      t.string :status

      t.timestamps
    end
  end
end
