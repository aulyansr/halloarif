class CreatePromos < ActiveRecord::Migration[5.1]
  def change
    create_table :promos do |t|
      t.string :name
      t.text :description
      t.string :type
      t.string :code

      t.timestamps
    end
  end
end
