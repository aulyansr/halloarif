# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

u = User.new
u.full_name = "Super Admin"
u.role = "SPADM"
u.email = "admin@halloarif.id"
u.password = "password"
u.skip_confirmation!
u.save(validate: false)

u = User.new
u.full_name = "Finance"
u.role = "MTV"
u.email = "admin@halloarif.id"
u.password = "password"
u.skip_confirmation!
u.save(validate: false)

ProblemCategory::Type.each do |x|
  u = ProblemCategory.new
  u.name = x
  u.save
end
