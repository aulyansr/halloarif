Rails.application.routes.draw do

  resources :videos
  resources :biodata
  resources :notification_data
  resources :testimonies
  resources :detail_motivators
  resources :schedules
  mount Ckeditor::Engine => '/ckeditor'
  get 'dashboard/index'
  get 'dashboard' => "dashboard#index", as: :dashboard
  get 'dashboard/partial_conver'
  get 'dashboard/partial_msgbox'
  get 'dashboard/partial_notif'
  get 'users/message_box'
  get 'messages/partial_conver'
  get 'pages/partial_conver'
  resources :messages


  root 'pages#index'

  resources :packages
  resources :conversations
  resources :before_conversation
  resources :before_box
  resources :after_register
  resources :promos
  resources :articles
  resources :article_categories, path: "kategori"
  resources :quotes
  resources :images
  resources :sliders
  resources :message_boxes
  resources :problem_categories
  resources :users
  get '/answer' => "pages#answer", as: :answer
  get '/answer-chat' => "pages#answer_chat", as: :answer_chat
  get '/rate-box' => "pages#rate_box", as: :rate_box
  get '/list_box' => "pages#list_box", as: :list_box
  get '/list_chat' => "pages#list_chat", as: :list_chat
  get '/user_agrement' => "pages#user_agrement", as: :user_agrement
  get '/intro' => "pages#intro", as: :intro
  get '/questioner' => "pages#questioner", as: :questioner
  get '/code-of-conduct' => "pages#conduct", as: :conduct
  get '/setup-payment' => "payments#init_payment", as: :init_payment
  get '/detail-payment' => "pages#payment", as: :payment_detail
  get '/beli-paket' => "conversations#init_conversation", as: :init_conversation
  get '/finish' => "conversations#finish", as: :finish
  get '/list-paket' => "pages#list_package", as: :list_package
  get '/about' => "pages#about", as: :about
  get '/contact' => "pages#contact", as: :contact
  get '/history' => "pages#history", as: :history
  get '/history-mtv' => "pages#history_mtv", as: :history_mtv
  get 'list-province' =>  "shared#province"
  get 'list-city' =>  "shared#city"
  get 'list-category' =>  "shared#problem_category"
  get 'validate_promo_code' => "payments#validate_promo_code"
  get '/payment-success' => "payments#payment_success"
  get '/payment-pending' => "payments#payment_pending"
  get '/payment-error' => "payments#payment_error"
  get 'init_box' => "message_boxes#init_box", as: :init_box
  get 'finish_box' => "pages#finish", as: :finish_message_box
  get 'info-motivator' => "pages#mtv_info", as: :mtv_info
  post 'payments/notify'

  get "/faq" => "dashboard#faq", :as => :faq
  get "/semua-artikel" => "pages#allstory", :as => :allstory
  post "/sendkeys" => "message_boxes#getJSON", :as => :getJSON
   post "/sendNo" => "message_boxes#sendPush", :as => :sendPush
  resources :payments
  devise_for :users ,
   path: '',
   path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register' }, :controllers => { :sessions => "sessions" }

   namespace :api, defaults: {format: :json} do
    namespace :v1 do
      devise_scope :user do
        post "sign_up", to: "registrations#create"
        post "sign_in", to: "sessions#create"
        delete "log_out", to: "sessions#destroy"
      end
        put "update_identity", to: "users#update_identity"
        resources :message_boxes
        put "add_rating/:id", to: "message_boxes#create_rating"
    end
  end
end
