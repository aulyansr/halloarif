namespace :db do
  namespace :populate do
    task all: [:admin, :finance, :human_capital, :motivator, :problem_category, :paket] do
    end

    task admin: :environment do
      User.client.destroy_all
      User.super_admin.destroy_all
      u = User.new
      u.full_name = "Super Admin"
      u.role = "SPADM"
      u.email = "admin@halloarif.co.id"
      u.password = "password"
      u.skip_confirmation!
      if u.save(validate: false)
        puts "Admin Created"
      else
        puts "Admin error: #{u.full_name} -> #{u.errors.first.join(": ")}"
      end
    end

    task finance: :environment do
      User.finance.destroy_all
      u = User.new
      u.full_name = "Finance"
      u.role = "FNC"
      u.email = "finance@halloarif.co.id"
      u.password = "password"
      u.skip_confirmation!
      if u.save(validate: false)
        puts "Finance Created"
      else
        puts "Finance error: #{u.full_name} -> #{u.errors.first.join(": ")}"
      end
    end

    task human_capital: :environment do
      User.human_capital.destroy_all
      u = User.new
      u.full_name = "Human Capital"
      u.role = "HMCPT"
      u.email = "human.capital@halloarif.co.id"
      u.password = "password"
      u.skip_confirmation!
      if  u.save(validate: false)
        puts "Human Capital Created"
      else
        puts "Human Capital error: #{u.full_name} -> #{u.errors.first.join(": ")}"
      end
    end

    task motivator: :environment do
      User.motivator.destroy_all

      4.times do |i|
        u = User.new
        u.motivator_type = "MOTIVATOR EKSKLUSIF"
        u.motivator_code = "GEMILANG-0#{'%02d' % (i + 1)}"
        u.role = "MTV"
        u.email = "gemilang#{'%02d' % (i + 1)}@halloarif.co.id"
        u.password = "password"
        u.skip_confirmation!
        if u.save(validate: false)
          puts "#{u.motivator_code} - #{u.email} Created"
          x = DetailMotivator.new
          x.user_id = u.id
          x.save
        else
          puts "#{u.motivator_code} error: #{u.full_name} -> #{u.errors.first.join(": ")}"
        end
      end
      16.times do |i|
        u = User.new
        u.motivator_type = "Motivator Inti"
        u.motivator_code = "GEMILANG-1#{'%02d' % (i + 1)}"
        u.role = "MTV"
        u.email = "gemilang#{'%02d' % (i + 5)}@halloarif.co.id"
        u.password = "password"
        u.skip_confirmation!
        if u.save(validate: false)
          puts "#{u.motivator_code} - #{u.email} Created"
          x = DetailMotivator.new
          x.user_id = u.id
          x.save
        else
          puts "#{u.motivator_code} error: #{u.full_name} -> #{u.errors.first.join(": ")}"
        end
      end
      15.times do |i|
        u = User.new
        u.motivator_type = "Motivator Jakarta"
        u.motivator_code = "GEMILANG-2#{'%02d' % (i + 1)}"
        u.role = "MTV"
        u.email = "gemilang#{'%02d' % (i + 21)}@halloarif.co.id"
        u.password = "password"
        u.skip_confirmation!
        if u.save(validate: false)
          puts "#{u.motivator_code} - #{u.email} Created"
          x = DetailMotivator.new
          x.user_id = u.id
          x.save
        else
          puts "#{u.motivator_code} error: #{u.full_name} -> #{u.errors.first.join(": ")}"
        end
      end
      11.times do |i|
        u = User.new
        u.motivator_type = "Motivator Metro"
        u.motivator_code = "GEMILANG-3#{'%02d' % (i + 1)}"
        u.role = "MTV"
        u.email = "gemilang#{'%02d' % (i + 37)}@halloarif.co.id"
        u.password = "password"
        u.skip_confirmation!
        if u.save(validate: false)
          puts "#{u.motivator_code} - #{u.email} Created"
          x = DetailMotivator.new
          x.user_id = u.id
          x.save
        else
          puts "#{u.motivator_code} error: #{u.full_name} -> #{u.errors.first.join(": ")}"
        end
      end
    end

    task problem_category: :environment do
      ProblemCategory.all.destroy_all
      ProblemCategory::Type.each do |x|
        u = ProblemCategory.new
        u.name = x
        if u.save
          puts "Category created: #{u.name}"
        else
          puts "Category error: #{u.name} -> #{u.errors.first.join(": ")}"
        end
      end
    end
    task paket: :environment do
      Package.all.destroy_all
      u = Package.new
      u.name = "Move On"
      u.description = " 5 Chat Respon Motivator ( masa aktif 3 hari )"
      u.price = 25000
      u.duration = 5
      if u.save
        puts "Package created: #{u.name}"
      else
        puts "Package error: #{u.name} -> #{u.errors.first.join(": ")}"
      end
      u = Package.new
      u.name = "Bangkit"
      u.description = "7 Chat Respon Motivator ( masa aktif 3 hari )"
      u.price = 35000
      u.duration = 7
      if u.save
        puts "Package created: #{u.name}"
      else
        puts "Package error: #{u.name} -> #{u.errors.first.join(": ")}"
      end
      u = Package.new
      u.name = "Berkarakter"
      u.description = "20 Chat Respon Motivator (masa aktif 5 hari )"
      u.price = 100000
      u.duration = 20
      if u.save
        puts "Package created: #{u.name}"
      else
        puts "Package error: #{u.name} -> #{u.errors.first.join(": ")}"
      end
      u = Package.new
      u.name = "Gemilang"
      u.description = "30 Chat Respon Motivator (masa aktif 7 hari )"
      u.price = 150000
      u.duration = 30
      if u.save
        puts "Package created: #{u.name}"
      else
        puts "Package error: #{u.name} -> #{u.errors.first.join(": ")}"
      end
    end

    task promo_mkt: :environment do
      6.times do |i|
        u = Promo.new
        u.name = "MKT-#{'%02d' % (i + 1)}"
        u.code = "MKT-#{'%02d' % (i + 1)}"
        if u.save
          puts "Promo created: #{u.name}"
        else
          puts "Promo error: #{u.name} -> #{u.errors.first.join(": ")}"
        end
      end
    end
    task promo_dkt: :environment do
      6.times do |i|
        u = Promo.new
        u.name = "DKT-#{'%02d' % (i + 1)}"
        u.code = "DKT-#{'%02d' % (i + 1)}"
        if u.save
          puts "Promo created: #{u.name}"
        else
          puts "Promo error: #{u.name} -> #{u.errors.first.join(": ")}"
        end
      end
    end

    task email_reminder_pagi: :environment do
      User.motivator.each do |i|
        NotifMailer.reminder_morning(i).deliver
        puts "#{i.motivator_code}"
      end
    end

    task schedule_reminder: :environment do
      Schedule.where(date: Date.today.strftime("%A").downcase).each do |i|
        if i.start.strftime("%I") == (Time.current).strftime("%I")
          NotifMailer.schedule_reminder(i.motivator).deliver
            puts "#{i.motivator.motivator_code}"
        end
      end
    end

    task change_password: :environment do
     User.motivator.each do |i|
       user = User.find(i.id)
        user.password = 'gemilang123'
        user.password_confirmation = 'gemilang123'
        user.save
      end
    end

  end
end
